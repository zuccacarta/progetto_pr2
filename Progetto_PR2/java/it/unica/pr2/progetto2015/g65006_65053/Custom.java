/*
  Lo scopo della classe Custom è quella di implementare una funzione, non presente 
  in LibreOffice, la quale permette di ordinare dal più grande al più piccolo i file di una 
  cartella di Dropbox e visualizzarli in base ad un vincolo di dimensione; 
  inoltre l'implementazione permette di evidenziare anche alcuni metadati.
*/

package it.unica.pr2.progetto2015.g65006_65053;
import it.unica.pr2.progetto2015.interfacce.SheetFunction;	
import com.dropbox.core.*;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.*;




public class Custom implements SheetFunction {
    	
	public String getAccessToken(String code) throws DbxException {
        	code = code.replace(" ", "");
        	return accessToken(code);
	}
    
    	/*restituisce l'AccessToken per un utente per poter accedere all'applicazione*/
	private static String accessToken(String code) throws DbxException{
        	final String APP_KEY = "w5tgkbcl0g5kvbr";
        	final String APP_SECRET = "opy3eheaod3mq45";
        	DbxAppInfo appInfo = new DbxAppInfo(APP_KEY, APP_SECRET);
		DbxWebAuthNoRedirect webAuth = new DbxWebAuthNoRedirect(getConfig(), appInfo);
        	String authorizeUrl = webAuth.start();
        	DbxAuthFinish authFinish = webAuth.finish(code);
        	String accessToken = authFinish.accessToken;
		return accessToken;
	}

	private static DbxRequestConfig getConfig(){
            return new DbxRequestConfig("Progetto pr2 Carta-Zucca", Locale.getDefault().toString());
     	}
    
	public Object execute(Object... args){
		String accessToken=(String)args[0];
		String str=(String)args[1];
		long bytes=(long)args[2];
		accessToken = accessToken.replace(" ", "");
		DbxClient utente=new DbxClient(getConfig(),accessToken);
		DbxEntry.WithChildren listing=null;
		try{		
			listing= utente.getMetadataWithChildren("/");
		} catch(DbxException e){}		
		int nFile=listing.children.size(); //numero di file contenuti nella cartella di dropbox
		Collections.sort(listing.children, new ByteComparator()); //ordina gli elementi della cartella				
		Object[][] file=new Object[3][nFile];
		for(int j=0;j<nFile;j++) { //inizializzo a stringa nulla gli elementi della matrice
			for(int i=0;i<3;i++)			
				file[i][j]="";	
		}	
		try{
			file=trovaFile(listing.children, str, bytes, file);
		} catch(DbxException e){}
		return file;
	}
	
	private static Object[][] trovaFile(List<DbxEntry> children, String str, long bytes, Object[][] matrFile) throws DbxException{
		int i=0;				
		switch(str){
			case(">"):
				for (DbxEntry child : children){			
					DbxEntry.File file = (DbxEntry.File)child;						
					if (file.numBytes>bytes) {
						matrFile[0][i] = file.name;
						matrFile[1][i] = file.humanSize;						
						matrFile[2][i] = file.lastModified;						
						i++;
					}
				}
				break;
			case(">="):
				for (DbxEntry child : children){			
					DbxEntry.File file = (DbxEntry.File)child;	
					if (file.numBytes>=bytes) {
						matrFile[0][i] = file.name;
						matrFile[1][i] = file.humanSize;
						matrFile[2][i] = file.lastModified;						
						i++;
					}
				}
				break;
			case("<"):
				for (DbxEntry child : children){			
					DbxEntry.File file = (DbxEntry.File)child;	
					if (file.numBytes<bytes) {
						matrFile[0][i] = file.name;
						matrFile[1][i] = file.humanSize;
						matrFile[2][i] = file.lastModified;						
						i++;
					}
				}
				break;
			case("<="):
				for (DbxEntry child : children){			
					DbxEntry.File file = (DbxEntry.File)child;	
					if (file.numBytes<=bytes) {
						matrFile[0][i] = file.name;
						matrFile[1][i] = file.humanSize;
						matrFile[2][i] = file.lastModified;						
						i++;
					}
				}
				break;
			case("="):
				for (DbxEntry child : children){			
					DbxEntry.File file = (DbxEntry.File)child;	
					if (file.numBytes==bytes) {
						matrFile[0][i] = file.name;
						matrFile[1][i] = file.humanSize;
						matrFile[2][i] = file.lastModified;
						i++;
					}
				}
				break;		
		}
		if(matrFile[0][0].equals(""))
			matrFile[0][0]="Non ci sono file di questa dimensione";		
		return matrFile;
	}

	@Override
	public final String getCategory() {
		return "Dropbox";
	}

    	/** Informazioni di aiuto */
    	@Override
    	public final String getHelp() {
		return "Trova i file di una cartella di dropbox in ordine di grandezza con il vincolo di una certa dimensione.";
	}

	@Override
	public final String getName() {
		return "CUSTOM";
	}
}
	
	

    
   
