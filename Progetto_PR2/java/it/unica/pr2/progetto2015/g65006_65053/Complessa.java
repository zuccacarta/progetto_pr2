package it.unica.pr2.progetto2015.g65006_65053;

import it.unica.pr2.progetto2015.interfacce.SheetFunction;

public class Complessa implements SheetFunction {
	
	@Override
	public Object execute(Object... args){
		int scelta;		
		Double somma1=0.0, somma2=0.0, media;
		String[][] matrice=(String[][])args[0];
		String[] intestazione= new String[matrice[0].length];
		for (int j=0;j<matrice[0].length;j++)
			intestazione[j]=matrice[0][j];
		String label=(String)args[1];
		Double[][] val=new Double[matrice.length][matrice[0].length];
		for(int i=1;i<matrice.length;i++) {
			for(int j=1;j<matrice[0].length;j++)
				val[i][j]=Double.parseDouble(matrice[i][j]);
		}
		String[][] matrice2=(String[][])args[2];
		int contatore=val.length-1;
		int ind=0;
		for (int k=1;k<matrice2[0].length;k++) {			
			if(intestazione[k].equals(label))
				ind=k;
		}		
		for (int j=1;j<matrice2[0].length;j++) {					
			if (!matrice2[1][j].equals("")) {
				char c=matrice2[1][j].charAt(0);
				char c2=matrice2[1][j].charAt(1);

				if(c=='>') {
					if(c2=='=') {
						Double num=Double.parseDouble(matrice2[1][j].substring(2,matrice2[1][j].length()));	
						for(int i=1;i<matrice.length;i++) {					
							if(val[i][ind]!=null && val[i][j]<num) {
								val[i][ind]=null;
								contatore--;
							}
						}
					} else {
						Double num=Double.parseDouble(matrice2[1][j].substring(1,matrice2[1][j].length()));
						for(int i=1;i<matrice.length;i++) {
							if(val[i][ind]!=null && val[i][j]<=num) {
								val[i][ind]=null;
								contatore--;
							}
						}
					}
				} else if(c=='<') {
					if(c2=='=') {
						Double num=Double.parseDouble(matrice2[1][j].substring(2,matrice2[1][j].length()));
						for(int i=1;i<matrice.length;i++) {
							if(val[i][ind]!=null && val[i][j]>num) {
								val[i][ind]=null;
								contatore--;
							}
						}
					} else {
						Double num=Double.parseDouble(matrice2[1][j].substring(1,matrice2[1][j].length()));
						for(int i=1;i<matrice.length;i++) {
							if(val[i][ind]!=null && val[i][j]>=num) {
								val[i][ind]=null;
								contatore--;
							}
						}
					}	
				} else 
					return 0.0;
			}
		}		
		for(int i=1; i<val.length; i++) {
			if(val[i][ind]!=null)
				somma1=somma1+val[i][ind];
		}
		media=somma1/contatore;
		for(int i=1;i<val.length; i++) {
			if(val[i][ind]!=null)					
				somma2=somma2+Math.pow(val[i][ind]-media,2);
		}
		return Math.sqrt(somma2/(contatore-1));	
	}
	
	@Override
	public final String getCategory() {
		return "Database";
	}

    	/** Informazioni di aiuto */
    	@Override
    	public final String getHelp() {
		return "Calcola la deviazione standard di un campione.";
	}

    	/**
	Nome della funzione.
	vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
	ad es. "VAL.DISPARI"
	*/
	@Override
	public final String getName() {
		return "DB.DEV.ST";
	}

}
