package it.unica.pr2.progetto2015.g65006_65053;

import java.util.Comparator;
import com.dropbox.core.*;

public class ByteComparator implements Comparator<DbxEntry> {
	
	public int compare(DbxEntry f1, DbxEntry f2) {
		if(((DbxEntry.File)f1).numBytes>((DbxEntry.File)f2).numBytes)
			return -1;
		else if(((DbxEntry.File)f1).numBytes<((DbxEntry.File)f2).numBytes)
			return 1;
		else
			return 0;
	}
}
