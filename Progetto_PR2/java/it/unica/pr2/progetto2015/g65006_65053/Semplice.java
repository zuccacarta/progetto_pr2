package it.unica.pr2.progetto2015.g65006_65053;

import it.unica.pr2.progetto2015.interfacce.SheetFunction;

public class Semplice implements SheetFunction {

	@Override
	public Object execute(Object... args) { 
		String stringa=new String();		
		if(args[0] instanceof String) {      			
			stringa=(String)args[0];
        		stringa=stringa.toLowerCase();
		}
		if(args[0] instanceof Integer) {
			stringa=args[0].toString();	
		} 
		return stringa;
	}

	@Override
	public final String getCategory() {
		return "Testo";
	}

	/** Informazioni di aiuto */
    	@Override
    	public final String getHelp() {
		return "Converte una stringa in minuscolo.";
	}

    	/**
    	Nome della funzione.
    	vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
    	ad es. "VAL.DISPARI"
    	*/
    	@Override
    	public final String getName() {
		return "MINUSC";
	}

}
