PROGETTO PR2 2015

GRUPPO 25
Membri:
	Fabio Carta - matricola: 65006
	Luigi Zucca - matricola: 65053

OBIETTIVO: Implementare 3 funzioni di LibreOffice utilizzando java con OBBA.

Classi utilizzate:
	- Semplice (implementa la funzione MINUSC);
	- Complessa (implementa la funzione DB.DEV.ST);
	- Custom (implementa una funzione non presente in LibreOffice sfruttando le librerie esterne contenenti gli API di Dropbox);
	- ByteComparator (classe utilizzata in Custom per ordinare i file di Dropbox dal più grande al più piccolo).
	
	I sorgenti delle classi e delle interfacce usate sono contenute in java/it/unica/pr2/g65006_65053.
	I compilati si trovano in build/progetto2015.jar.
	Le librerie esterne utilizzate sono contenute nella cartella libs.

Descrizione delle funzioni:
SEMPLICE
	La classe Semplice implementa una funzione LibreOffice semplice, ovvero che non restituisce un array.
	Nel nostro caso implementa la funzione testo MINUSC che converte il testo di una stringa in minuscolo.
	La funzione prende in input una stringa "Testo" e restituisce una stringa.
	
	SINTASSI ORIGINALE LIBREOFFICE
		MINUSC(Testo)
	SINTASSI OBBA
		OBGET(OBCALL("result";OBMAKE("oggetto";"it.unica.pr2.progetto2015.g65006_65053.Semplice");"execute";OBMAKE("a";"String;	
		Testo)))
	dove "result", "oggetto" e "a" sono etichette, it.unica.pr2.progetto2015.g65006_65053.Semplice è il pacchetto in cui è contenuta la
	classe Semplice, execute è il metodo della classe da eseguire.

COMPLESSA
	La classe Complessa implementa una funzione LibreOffice piu' complessa, ovvero che restituisce un array o che richiede comunque uno
	sforzo maggiore nell'implementazione.
	Nel nostro caso implementa la funzione database DB.DEV.ST che calcola la deviazione standard della popolazione sulla base di un
	campione.
	La funzione prende in input una matrice "Database", una stringa "Campo database" che è il campo del database scelto e una matrice
 	"Criteri di ricerca" per i criteri di ricerca scelti dall'utente e restituisce un valore che rappresenta la deviazione standard.

	SINTASSI ORIGINALE LIBREOFFICE
		DB.DEV.ST(Database; Campo database; Criteri di ricerca)
	SINTASSI OBBA
		OBGET(OBCALL("resultComplessa";OBMAKE("oggetto";"it.unica.pr2.progetto2015.g65006_65053.Complessa");"execute";OBMAKE
		("matrice";"String[][]";Database);OBMAKE("label";"String";Campo database);OBMAKE("matrice2";"String[][]";Criteri di
		ricerca)))
	dove "resultComplessa", "matrice", "label", "matrice2" sono etichette, it.unica.pr2.progetto2015.g65006_65053.Semplice è il
	pacchetto in cui è contenuta la classe Complessa, execute è il metodo della classe da eseguire, Database e Criteri di ricerca due 
	matrici di stringhe.

CUSTOM
	La classe Custom implementa una funzione non presente in LibreOffice sfruttando le librerie esterne contenenti le API di Dropbox.
	La funzione visualizza i file della cartella di Dropbox Applicazioni/Progetto PR2 Carta-Zucca in ordine di dimensione dal più grande
	al più piccolo, tramite la classe ByteComparator, e in base ad un vincolo di dimensione scelto dall'utente.
	
	IMPORTANTE:
	- Entrare nel sito https://www.dropbox.com/1/oauth2/authorize?locale=en_US&client_id=w5tgkbcl0g5kvbr&response_type=code, 		  effettuare il login al proprio dropbox e cliccare allow per ottenere il codice da inserire nella cella C7. Al primo inserimento 		  del codice verrà creata sul proprio Dropbox la cartella Applicazioni/Progetto PR2 Carta-Zucca su cui inserire i file che verranno 		  poi visualizzati in ordine di dimensione in LibreOffice.
	- La matrice è impostata per visualizzare massimo 10 file ed è composta dai nomi dei file, dimensione e ultima modifica.
	- L'AccessToken per poter accedere ai file della cartella verrà generato automaticamente dalla funzione usando il codice inserito
	  dall'utente.
	- L'utente deve inserire i vincoli di dimensione in base a cui verranno scelti i file da visualizzare:
		Nella cella C13 inserire un simbolo tra maggiore(>), minore(<), maggiore-uguale(>=), minore-uguale(<=) o uguale(=).
		Nella cella C14 inserire la dimensione in bytes.
	
Il progetto è testato su LibreOffice 4.2.8, jdk 1.8, Obba 4.2.2.
 
	
